Author : Abubakr Sokarno  |
Date   : 2018-09-15       |
==========================| 

What is This ?
---------------
- This is a crawler for fetching links inside any website link that contains pagination.
- you can change website name from config.php 
    or you can make a table that fetch from it website with its needed configuration.

How to use it ?
----------------
- about database, the sql file uploaded on repo, 
    just contains one table that holds the extracted urls its name extracted urls,
    with the f0110wing structure :

    = id, PRIMARY, INT , AUT0 INCREMENT
    = parent_url, VARCHAR(255)
    = extracted_url, VARCHAR(255), INDEX , UNIQUE // in order not to ask before adding link, 
                                                  // just insert link between try and catch to catch the duplicated links added
    = crawl_time , TIMESTAMP
    = parse_time , TIMESTAMP

- about running, open cmd or terminal in the project directory and type -> php crawler.php 

