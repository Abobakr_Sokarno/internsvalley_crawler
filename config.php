<?php

return array(
    "servername"    => "127.0.0.1",
    "username"      => "root" ,
    "password"  => "",
    "database_name" => "internsvalley",
    "MAX_DEPTH"     => 1 ,
    "NUMBER_OF_PAGES" => 2,
    "default_website" => "https://www.homegate.ch/mieten/immobilien/kanton-zuerich/trefferliste",
    "domain_delimeter" => ".ch",
    "pagination_param" => "ep=",
    "explode_keyword" => "/mieten/"
);