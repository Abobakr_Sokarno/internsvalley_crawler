<?php
include('simpleDom/simple_html_dom.php');
$config = include("config.php");

/**********  Some configuration variables **************/
$servername = $config['servername'] ;
$username = $config['username'];
$password = $config['password'] ;
$databaseName = $config['database_name'] ;

/******* Current time which will be the start time, not changed until the script ends ********/
$global_time = date("Y-m-d H:i:s") ;  

/**
 * Get and Store links fetched from current url
 * 
 * @param $base_url example : https://www.homegate.ch
 * @param $parent_url example : https://www.homegate.ch/mieten/immobilien/kanton-zuerich/trefferliste?ep=4
 * @param $link example : https://www.homegate.ch/mieten/108844702
 */
function storeLinks($base_url,$parent_url,$link) {
    $dom = new domDocument;
    @$dom->loadHTML(file_get_contents($link));
    $links = $dom->getElementsByTagName('a');
    foreach ($links as $tag) {
        $extracted_url = $tag->getAttribute('href') ;
        if(filter($base_url,$extracted_url))
            insertRecordToDB($parent_url,$base_url.$extracted_url);
    }
}

function curl_get_contents($url)
{
    $ch = curl_init();
    $agents = array(
        'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.7; rv:7.0.1) Gecko/20100101 Firefox/7.0.1',
        'Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.9.1.9) Gecko/20100508 SeaMonkey/2.0.4',
        'Mozilla/5.0 (Windows; U; MSIE 7.0; Windows NT 6.0; en-US)',
        'Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10_6_7; da-dk) AppleWebKit/533.21.1 (KHTML, like Gecko) Version/5.0.5 Safari/533.21.1'
    );
    curl_setopt($ch,CURLOPT_USERAGENT,$agents[array_rand($agents)]);
    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);

    $data = curl_exec($ch);
    curl_close($ch);

    return $data;
}

/**
 * Insert extracted links to database 
 * 
 * @param $parent_url parent url of extracted url, from where extracted link this is its parent
 * @param $extracted_url is the link fetched 
 */
function insertRecordToDB($parent_url,$extracted_url){
    $servername = $GLOBALS['servername'] ;
    $username = $GLOBALS['username'];
    $password = $GLOBALS['password'] ;
    $databaseName = $GLOBALS['databaseName'];
    $current_time = date('Y-m-d H:i:s');
    $crawl_time = $GLOBALS['global_time'] ; 

    try{
        $conn = new PDO("mysql:host=$servername;dbname=$databaseName;charset=utf8", $username, $password);
        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $stmt = $conn->prepare("INSERT INTO extracted_urls(parent_url,extracted_url,crawl_time,parse_time)
        VALUES (:parent_url,:extracted_url,:crawl_time,:parse_time)");
        $stmt->bindParam(':parent_url', $parent_url);
        $stmt->bindParam(':extracted_url', $extracted_url);
        $stmt->bindParam(':crawl_time', $crawl_time);
        $stmt->bindParam(':parse_time', $current_time);
        $stmt->execute();
    } catch (PDOException $e){

    }
}

/**
 * The filter that accept the link or not 
 * 
 * @param $main_link the base link that will be compared with extracted link
 * @param $extracted_url the fetched url 
 * @return modified link if can be inserted, and follows the rules 
 * @return false if it belongs to rejected list or have domain name not equal current website domain name
 */

function filter($main_link,$link){
    $modified_link = NULL ;
    if($link[0]=="/") {
        if(substr($main_link,-1) == "/") {
            $link2 = substr($link,1);
            $modified_link = $main_link . $link2;  
        }
        else {
            $modified_link = $main_link . $link ; 
        }
    } 
    else {
        $get_http = substr($link,0,4);
        if($get_http == "http") {
            if(strpos($link,$main_link) === false) {
                $modified_link = false; 
            }
            else {
                $modified_link = $link ; 
            }
        } 
        else {
            if(substr($main_link,-1) == "/") {
                $modified_link = $main_link . $link;  
            }
            else {
                $modified_link = $main_link ."/". $link ; 
            }
        }
    }

    if( isArticle($modified_link) )
        return $modified_link ;
    return false ;
}

function isArticle($link) {
    $parts = explode($GLOBALS['config']['explode_keyword'],$link); 
    if(count($parts) >= 2) {
        $get_id = explode("?",$parts[1]); 
        if(count($get_id) > 1) {
            if(is_numeric($get_id[0])) {
                return true ; 
            }
        }
        else {
            if(is_numeric($parts[1])) {
                return true ; 
            }
        }
    }
    return false ;
}

/**
 * Loop through website pages 
 * 
 * @param $root_url that will be incremented to next 50 pages
 */
function getSitePages($root_url) {  
    $NUMBER_OF_PAGES = $GLOBALS['config']['NUMBER_OF_PAGES'] ; 
    $delimeter = $GLOBALS['config']['domain_delimeter']; 
    $pagination_param = $GLOBALS['config']['pagination_param']; 
    $base_url = explode($delimeter,$root_url);
    $base_url = $base_url[0].$delimeter;
    for ($i = 1 ; $i <= $NUMBER_OF_PAGES; $i++ ) {
        $links = storeLinks($base_url,$root_url,$root_url."?".$pagination_param.$i);
    }
}

/******* main() *********/
try{
    $default_url = $config['default_website']; 
    echo "Start crawling ...\n";
    getSitePages($default_url,$default_url);
    echo "end\n";
} catch (PDOException $e) {

}
exit();